const router = require("express").Router(); // Importa o módulo Router do Express

const userController = require('../controller/UserController')
const dbController = require("../controller/dbController"); // Importa o controlador dbController
const quadraController = require("../controller/quadraController");// Importa o controlador quadraController
const ReservaController = require('../controller/reservaController'); // Importa o controlador ReservaController


//Rotas do quadraController
router.post('/quadrasPost/', quadraController.postQuadra)
router.put('/quadrasPut/:id', quadraController.updateQuadra)
router.delete('/quadrasDelete/:id', quadraController.deleteQuadraId)
router.get('/quadrasGet/', quadraController.getQuadra)
router.get('/quadrasGet/:id', quadraController.getQuadraById)

//Rotas do userController
router.post('/usuarioPost/', userController.criarUsuario );
router.get('/usuarioGet/', userController.buscarUsuarios);
router.get('/usuarioGet/:id', userController.getAllReservaByID);
router.get('/usuarios/:id', userController.verificarReservaUsuario);

router.post('/postLogin',userController.postLogin )
router.put('/AtualizarUsuario/:id', userController.atualizarUsuario);
router.delete('/DeletarUsuario/:id', userController.deletarUsuario);


// Rotas de reservaController CRUD
router.post('/reservas', ReservaController.postReserva); // Rota para criar nova reserva
router.delete('/reservas/:id', ReservaController.deleteReserva); // Rota para deletar reserva
router.put('/reservas/', ReservaController.updateReserva); // Rota para atualizar reserva existente
router.get('/reservas/:id', ReservaController.getAllReservaByID); // Rota para obter reserva por ID
router.get('/reservas', ReservaController.getAllReserva); // Rota para obter todas as reservas
router.get("/reservasdescription/:id", ReservaController.getQuadrasPorUsuario);


//Rotas do dbController
router.get("/tables", dbController.getNameTables); // Rota para consultar os nomes das tabelas do banco de dados
router.get('/tablesdescription', dbController.getTablesDescription); // Rota para consultar as descrições das tabelas do banco de dados

module.exports = router; // Exporta o objeto router com as rotas definidas
