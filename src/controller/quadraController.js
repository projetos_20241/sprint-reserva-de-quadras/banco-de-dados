const connect = require("../db/connect");

// Classe controladora para gerenciar as quadras esportivas
module.exports = class QuadraController {
  // Método para criar uma nova quadra esportiva
  static async postQuadra(req, res) {
    // Extração de dados do corpo da requisição
    const { TipoQuadra, Capacidade, ValorPorHora } = req.body;

    // Validação dos campos obrigatórios
    if (!TipoQuadra || !Capacidade || !ValorPorHora) {
      return res
        .status(400)
        .json({ error: "Campos obrigatórios não preenchidos" });
    }

    // Query para inserir uma nova quadra no banco de dados
    const query = `INSERT INTO AluguelQuadra (TipoQuadra, Capacidade, ValorPorHora) VALUES (?, ?, ?)`;

    // Execução da query de inserção
    connect.query(
      query,
      [TipoQuadra, Capacidade, ValorPorHora],
      function (err, results) {
        if (err) {
          console.error(err); // Log do erro no servidor
          return res.status(500).json({ error: "Erro interno do servidor" });
        }
        console.log("Quadra cadastrada no MySQL"); // Log de sucesso no servidor
        return res
          .status(201)
          .json({ message: "Quadra cadastrada com sucesso" });
      }
    );
  }

  // Método para atualizar uma quadra existente
  static async updateQuadra(req, res) {
    // Extração de dados da requisição e do corpo da requisição
    const { id } = req.params;
    const { TipoQuadra, Capacidade, ValorPorHora } = req.body;

    // Validação dos campos obrigatórios
    if (!TipoQuadra || !Capacidade || !ValorPorHora) {
      return res
        .status(400)
        .json({ error: "Campos obrigatórios não preenchidos" });
    }

    // Query para atualizar uma quadra existente no banco de dados
    const query =
      "UPDATE AluguelQuadra SET TipoQuadra = ?, Capacidade = ?, ValorPorHora = ? WHERE QuadraID = ?";

    // Execução da query de atualização
    connect.query(
      query,
      [TipoQuadra, Capacidade, ValorPorHora, id],
      function (err, results) {
        if (err) {
          console.error("Erro ao executar a query:", err); // Log do erro no servidor
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verificação se a quadra foi encontrada e atualizada
        if (results.affectedRows === 0) {
          return res.status(404).json({ error: "Quadra não encontrada" });
        }

        return res
          .status(200)
          .json({ message: "Quadra atualizada com sucesso" });
      }
    );
  }

  // Método para excluir uma quadra pelo ID
  static async deleteQuadraId(req, res) {
    // Extração do ID da quadra da requisição
    const { id } = req.params;

    // Validação do ID fornecido
    if (!id || isNaN(id) || parseInt(id) <= 0) {
      return res.status(400).json({ error: "ID de Quadra inválido" });
    }

    // Query para excluir uma quadra do banco de dados
    const query = "DELETE FROM AluguelQuadra WHERE QuadraID = ?";

    // Execução da query de exclusão
    connect.query(query, [id], function (err, results) {
      if (err) {
        console.error("Erro ao executar a query:", err); // Log do erro no servidor
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      // Verificação se a quadra foi encontrada e excluída
      if (results.affectedRows === 0) {
        return res.status(404).json({ error: "Quadra não encontrada" });
      }

      return res.status(200).json({ message: "Quadra removida com sucesso" });
    });
  }

  // Método para obter informações de várias quadras
  static async getQuadra(req, res) {
    // Query para selecionar todas as quadras no banco de dados
    const query = `SELECT * FROM AluguelQuadra`;

    // Execução da query de seleção
    connect.query(query, function (err, results) {
      if (err) {
        console.error(err); // Log do erro no servidor
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      return res
        .status(200)
        .json({ message: "Obtendo todas as quadras", quadras: results });
    });
  }

  // Método para obter uma quadra pelo ID
  static async getQuadraById(req, res) {
    // Extração do ID da quadra da requisição
    const { id } = req.params;

    // Validação do ID fornecido
    if (!id || isNaN(id)) {
      return res.status(400).json({ error: "ID inválido" });
    }

    // Query para selecionar uma quadra específica no banco de dados
    const query = `SELECT * FROM AluguelQuadra WHERE QuadraID = ?`;

    // Execução da query de seleção
    connect.query(query, [id], function (err, results) {
      if (err) {
        console.error(err); // Log do erro no servidor
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      // Verificação se a quadra foi encontrada
      if (results.length === 0) {
        return res.status(404).json({ error: "Quadra não encontrada" });
      }

      return res
        .status(200)
        .json({ message: "Obtendo quadra com ID: " + id, quadra: results[0] });
    });
  }
};
