const connect = require("../db/connect");

// Classe controladora para gerenciar as reservas
class ReservaController {
  // Método para criar uma nova reserva
  static async postReserva(req, res) {
    // Extração dos dados do corpo da requisição
    const {
      UsuarioID,
      QuadraID,
      TipoQuadra,
      Data,
      HoraInicial,
      HoraFinal,
      ValorPorHora,
    } = req.body;

    // Verificação se todos os campos obrigatórios foram preenchidos
    if (
      !UsuarioID ||
      !QuadraID ||
      !TipoQuadra ||
      !Data ||
      !HoraInicial ||
      !HoraFinal ||
      !ValorPorHora
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Verificação se a hora de início é anterior à hora de término
    if (new Date(HoraInicial) >= new Date(HoraFinal)) {
      return res.status(400).json({
        error: "A hora de início deve ser anterior à hora de término",
      });
    }

    // Consulta para verificar se já existe uma reserva para a mesma quadra na mesma data e horário
    const query = `SELECT * FROM reservas WHERE QuadraID = ? AND Data = ? AND ((HoraInicial < ? AND HoraFinal > ?) OR (HoraInicial >= ? AND HoraInicial < ?) OR (HoraFinal > ? AND HoraFinal <= ?))`;
    connect.query(
      query,
      [
        QuadraID,
        Data,
        HoraFinal,
        HoraInicial,
        HoraInicial,
        HoraFinal,
        HoraInicial,
        HoraFinal,
      ],
      function (err, result) {
        if (err) {
          console.error("Erro ao verificar reservas existentes:", err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }
        if (result.length > 0) {
          return res.status(400).json({
            error:
              "Já existe uma reserva para esta quadra nesta hora e neste dia",
          });
        }

        // Cálculo do valor total da reserva
        const inicio = new Date(`${Data}T${HoraInicial}`);
        const fim = new Date(`${Data}T${HoraFinal}`);
        const horasReservadas = (fim - inicio) / (1000 * 60 * 60); // Conversão de milissegundos para horas
        const valorTotal = horasReservadas * ValorPorHora;

        // Inserção da nova reserva no banco de dados
        const queryInsert = `INSERT INTO reservas (UsuarioID, QuadraID, TipoQuadra, Data, HoraInicial, HoraFinal, ValorPorHora, ValorTotal) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;
        connect.query(
          queryInsert,
          [
            UsuarioID,
            QuadraID,
            TipoQuadra,
            Data,
            HoraInicial,
            HoraFinal,
            ValorPorHora,
            valorTotal,
          ],
          function (err) {
            if (err) {
              console.log(err);
              res.status(500).json({ error: "Erro ao cadastrar reserva" });
              return;
            }
            console.log("Reserva cadastrada com sucesso");
            res.status(201).json({
              message: "Reserva cadastrada com sucesso",
              valorTotal: valorTotal,
            });
          }
        );
      }
    );
  }

  // Método para obter todas as reservas
  static async getAllReserva(req, res) {
    try {
      // Query para selecionar todas as reservas no banco de dados
      const query = "SELECT * FROM reservas";
      connect.query(query, function (err, result) {
        if (err) {
          console.error("Erro ao obter reservas:", err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }
        console.log("Reservas obtidas com sucesso");
        res
          .status(200)
          .json({ message: "Obtendo todas as reservas", reservas: result });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para obter uma reserva específica pelo ID
  static async getAllReservaByID(req, res) {
    // Extração do ID da reserva dos parâmetros da requisição
    const ReservaID = req.params.id;

    try {
      // Query para selecionar uma reserva específica pelo ID no banco de dados
      const query = `SELECT * FROM reservas WHERE ReservaID = ?`;
      connect.query(query, [ReservaID], function (err, result) {
        if (err) {
          console.error("Erro ao obter reserva:", err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verificação se a reserva foi encontrada
        if (result.length === 0) {
          return res.status(404).json({ error: "Reserva não encontrada" });
        }

        console.log("Reserva obtida com sucesso");
        res.status(200).json({
          message: "Obtendo a reserva com ID: " + ReservaID,
          reserva: result[0],
        });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para atualizar uma reserva existente
  static async updateReserva(req, res) {
    // Extração do ID da reserva dos parâmetros da requisição e dos dados do corpo da requisição
    const ReservaID = req.params.id;
    const {
      UsuarioID,
      QuadraID,
      TipoQuadra,
      Data,
      HoraInicial,
      HoraFinal,
      ValorPorHora,
    } = req.body;

    // Verificação se todos os campos obrigatórios foram preenchidos
    if (
      !UsuarioID ||
      !QuadraID ||
      !TipoQuadra ||
      !Data ||
      !HoraInicial ||
      !HoraFinal ||
      !ValorPorHora
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Verificação se a hora de início é anterior à hora de término
    if (new Date(HoraInicial) >= new Date(HoraFinal)) {
      return res.status(400).json({
        error: "A hora de início deve ser anterior à hora de término",
      });
    }

    // Consulta para verificar se já existe uma reserva para a mesma quadra na mesma data e horário
    const query = `SELECT * FROM reservas WHERE QuadraID = ? AND Data = ? AND ((HoraInicial < ? AND HoraFinal > ?) OR (HoraInicial >= ? AND HoraInicial < ?) OR (HoraFinal > ? AND HoraFinal <= ?))`;
    connect.query(
      query,
      [
        QuadraID,
        Data,
        HoraFinal,
        HoraInicial,
        HoraInicial,
        HoraFinal,
        HoraInicial,
        HoraFinal,
      ],
      function (err, result) {
        if (err) {
          console.error("Erro ao verificar reservas existentes:", err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }
        if (result.length > 0) {
          return res.status(400).json({
            error:
              "Já existe uma reserva para esta quadra nesta hora e neste dia",
          });
        }

        // Cálculo do valor total da reserva
        const inicio = new Date(`${Data}T${HoraInicial}`);
        const fim = new Date(`${Data}T${HoraFinal}`);
        const horasReservadas = (fim - inicio) / (1000 * 60 * 60); // Conversão de milissegundos para horas
        const valorTotal = horasReservadas * ValorPorHora;

        // Atualização da reserva no banco de dados
        const queryUpdate = `UPDATE reservas SET UsuarioID = ?, QuadraID = ?, TipoQuadra = ?, Data = ?, HoraInicial = ?, HoraFinal = ?, ValorPorHora = ?, ValorTotal = ? WHERE ReservaID = ?`;
        connect.query(
          queryUpdate,
          [
            UsuarioID,
            QuadraID,
            TipoQuadra,
            Data,
            HoraInicial,
            HoraFinal,
            ValorPorHora,
            valorTotal,
            ReservaID,
          ],
          function (err) {
            if (err) {
              console.error("Erro ao atualizar reserva:", err);
              res.status(500).json({ error: "Erro interno do servidor" });
              return;
            }
            console.log("Reserva atualizada com sucesso");
            res.status(200).json({
              message: "Reserva atualizada com sucesso",
              valorTotal: valorTotal,
            });
          }
        );
      }
    );
  }

  // Método para deletar uma reserva
  static async deleteReserva(req, res) {
    // Extração do ID da reserva dos parâmetros da requisição
    const ReservaID = req.params.id;

    // Query para deletar uma reserva no banco de dados
    const query = `DELETE FROM reservas WHERE ReservaID = ?`;

    connect.query(query, [ReservaID], function (err) {
      if (err) {
        console.error("Erro ao deletar reserva:", err);
        res.status(500).json({ error: "Erro interno do servidor" });
        return;
      }
      console.log("Reserva deletada com sucesso");
      res.status(200).json({ message: "Reserva deletada com sucesso" });
    });
  }

  // Método para obter quadras reservadas por usuário
  static async getQuadrasPorUsuario(req, res) {
    // Query para selecionar quadras reservadas por usuário
    const query = `
    SELECT u.UsuarioID, u.NomeUsuario, a.QuadraID, a.TipoQuadra, r.Data, r.HoraInicial, r.HoraFinal
    FROM Reservas r
    JOIN Usuario u ON r.UsuarioID = u.UsuarioID
    JOIN AluguelQuadra a ON r.QuadraID = a.QuadraID
    ORDER BY u.UsuarioID, r.Data, r.HoraInicial;
    `;

    connect.query(query, function (err, result) {
      if (err) {
        console.error("Erro ao obter quadras reservadas por usuário:", err);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }
      console.log("Quadras reservadas por usuário obtidas com sucesso");
      res.status(200).json({
        message: "Obtendo quadras reservadas por usuário",
        reservas: result,
      });
    });
  }
}

module.exports = ReservaController;
