const connect = require("../db/connect");

module.exports = class UsuarioController {
  static async criarUsuario(req, res) {
    const { NomeUsuario, Email, CPF, Senha } = req.body;

    // Verificar se todos os campos obrigatórios foram preenchidos
    if (!CPF || !Email || !Senha || !NomeUsuario) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Verificar se o CPF é um número e tem exatamente 11 dígitos
    else if (isNaN(CPF) || CPF.length !== 11) {
      return res
        .status(400)
        .json({
          error: "CPF inválido. Deve conter exatamente 11 dígitos numéricos",
        });
    }

    // Verificar se o email contém o caractere "@"
    else if (!Email.includes("@")) {
      return res.status(400).json({ error: "Email inválido. Deve conter @" });
    } else {
      // Query para inserir um novo usuário na tabela Usuario
      const query = `INSERT INTO Usuario (NomeUsuario, Email, CPF, Senha) VALUES (?, ?, ?, ?)`;

      connect.query(
        query,
        [NomeUsuario, Email, CPF, Senha],
        function (err, results) {
          if (err) {
            // Verificar se o erro é de entrada duplicada (CPF já cadastrado)
            if (err.code === "ER_DUP_ENTRY") {
              console.error(err);
              return res.status(400).json({ error: "CPF já cadastrado" });
            } else {
              console.error(err);
              return res
                .status(500)
                .json({ error: "Erro interno do servidor" });
            }
          }
          console.log("Inserido no MySQL");
          return res
            .status(201)
            .json({ message: "Usuário criado com sucesso" });
        }
      );
    }
  }

  static async buscarUsuarios(req, res) {
    // Query para selecionar todos os usuários da tabela Usuario
    const query = `SELECT * FROM Usuario`;

    connect.query(query, function (err, results) {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      return res
        .status(200)
        .json({ message: "Obtendo todos os usuários", users: results });
    });
  }

  static async getAllReservaByID(req, res) {
    const UsuarioID = req.params.id;

    try {
      // Query para selecionar um usuário específico pelo ID
      const query = "SELECT * FROM usuario WHERE UsuarioID = ?";
      connect.query(query, [UsuarioID], function (err, result) {
        if (err) {
          console.error("Erro ao obter usuário:", err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verificar se o usuário foi encontrado
        if (result.length === 0) {
          return res.status(404).json({ error: "Usuário não encontrado" });
        }

        console.log("Usuário obtido com sucesso");
        res
          .status(200)
          .json({
            message: "Obtendo usuário com ID: " + UsuarioID,
            usuario: result[0],
          });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async atualizarUsuario(req, res) {
    const id = req.params.id;
    const { NomeUsuario, Email, CPF, Senha } = req.body;

    // Verificar se todos os campos obrigatórios foram preenchidos
    if (!CPF || !Email || !Senha || !NomeUsuario) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Checar se o ID é um número válido
    if (isNaN(id)) {
      return res.status(400).json({ error: "ID inválido" });
    }

    // Query para atualizar os dados do usuário na tabela Usuario
    const query =
      "UPDATE Usuario SET CPF = ?, Email = ?, Senha = ?, NomeUsuario = ? WHERE UsuarioID = ?";

    connect.query(
      query,
      [CPF, Email, Senha, NomeUsuario, id],
      function (err, results) {
        if (err) {
          console.error("Erro ao executar a query:", err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verificar se o usuário foi encontrado e atualizado
        if (results.affectedRows === 0) {
          return res.status(404).json({ error: "Usuário não encontrado" });
        }

        return res
          .status(200)
          .json({ message: "Usuário atualizado com ID: " + id });
      }
    );
  }

  static async deletarUsuario(req, res) {
    const id = req.params.id;

    // Verificar se o ID está presente e se é um número válido
    if (!id) {
      return res.status(400).json({ error: "ID do usuário não fornecido" });
    }

    // Checar se o ID é um número válido
    if (isNaN(id)) {
      return res.status(400).json({ error: "ID inválido" });
    }

    // Query para deletar um usuário específico pelo ID
    const query = "DELETE FROM Usuario WHERE UsuarioID = ?";

    connect.query(query, [id], function (err, results) {
      if (err) {
        console.error("Erro ao executar a query:", err);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      // Verificar se o usuário foi encontrado e deletado
      if (results.affectedRows === 0) {
        return res.status(404).json({ error: "Usuário não encontrado" });
      }

      return res
        .status(200)
        .json({ message: "Usuário excluído com ID: " + id });
    });
  }

  static async postLogin(req, res) {
    const { Email, Senha } = req.body;

    // Verificar se o email e a senha foram fornecidos
    if (!Email || !Senha) {
      return res.status(400).json({ error: "Email e Senha são obrigatórios" });
    }

    // Query para selecionar um usuário pelo email e senha (login)
    const query = `SELECT * FROM Usuario WHERE Email = ? AND Senha = ?`;

    connect.query(query, [Email, Senha], function (err, results) {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      // Verificar se as credenciais estão corretas
      if (results.length === 0) {
        return res.status(401).json({ error: "Credenciais inválidas" });
      }

      return res
        .status(200)
        .json({ message: "Login realizado com sucesso", user: results[0] });
    });
  }
  static async verificarReservaUsuario(req, res) {
    const UsuarioID = req.params.id;

    // Verificar se o ID do usuário está presente e se é um número válido
    if (!UsuarioID || isNaN(UsuarioID)) {
      return res.status(400).json({ error: "ID do usuário inválido" });
    }

    // Query para verificar se o usuário tem alguma reserva
    const query = `SELECT * FROM Reservas WHERE UsuarioID = ?`;

    connect.query(query, [UsuarioID], function (err, results) {
      if (err) {
        console.error(err);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }

      // Verificar se o usuário tem alguma reserva
      if (results.length === 0) {
        return res.status(404).json({ message: "Usuário não possui reservas" });
      }

      return res
        .status(200)
        .json({ message: "Usuário possui reservas", reservas: results });
    });
  }
};
